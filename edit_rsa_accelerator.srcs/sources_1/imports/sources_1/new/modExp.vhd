----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 11/05/2018 11:53:06 PM
-- Design Name: 
-- Module Name: modExp - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
use ieee.std_logic_unsigned.all;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity modExp is
	generic (
 monExp_SIZE          : integer := 256
);
port (
-----------------------------------------------------------------------------
-- Clocks and reset
-----------------------------------------------------------------------------      
clk                                          :  in std_logic;
reset_n                                      :  in std_logic;
---------------------------------------------------
---           Slave -> Master
---------------------------------------------------
msgin_valid                                  : in std_logic; -- is data ready?
msgin_last                                   : in std_logic;
msgin_ready                                  : out std_logic; -- we are ready to recieve
---------------------------------------------------
---           Master -> slave
---------------------------------------------------
msgout_valid                                 : out std_logic;   -- we have ready data to send
msgout_ready                                 : in std_logic; -- are they ready to recieve?
msgout_data                                  : out std_logic_vector(monExp_SIZE-1 downto 0);
msgout_last                                  : out std_logic;
-----------------------------------------------------------------------------
-- Interface to the register block
-----------------------------------------------------------------------------
msgin_data              :  in std_logic_vector(monExp_SIZE-1 downto 0);    
key_e_d                 :  in std_logic_vector(monExp_SIZE-1 downto 0);
key_n                   :  in std_logic_vector(monExp_SIZE-1 downto 0);
key_rmodn               :  in std_logic_vector(monExp_SIZE-1 downto 0);
key_rrmodn              :  in std_logic_vector(monExp_SIZE-1 downto 0)   
   
);
end modExp;

architecture Behavioral of modExp is


-----------------------------------------------------------------------------
-- FSM control
-----------------------------------------------------------------------------     
signal msgin_ready_temp : std_logic;
signal msgin_data_temp              : std_logic_vector(monExp_SIZE-1 downto 0); 
-----------------------------------------------------------------------------
-- FSM control
-----------------------------------------------------------------------------     
signal resetMainCounter                             : std_logic;
signal final_cnt_dne                                : std_logic;
-----------------------------------------------------------------------------
-- Counter
-----------------------------------------------------------------------------
signal cnt                                          : std_logic_vector(8 downto 0);
signal enableMainCounter                            : std_logic;
-----------------------------------------------------------------------------
-- Internal signals
-----------------------------------------------------------------------------
signal key_ei                                       : std_logic_vector(monExp_SIZE-1 downto 0);
signal monproCaseSignal                             : std_logic_vector(monExp_SIZE-1 downto 0);
signal xm_reg                                       : std_logic_vector(monExp_SIZE-1 downto 0);
signal xm_reg_new                                   : std_logic_vector(monExp_SIZE-1 downto 0);
signal xmreg_rmodn                                  : std_logic;
-----------------------------------------------------------------------------
-- Monpro (M, r2modn)
-----------------------------------------------------------------------------
signal monpro_0_A                                   : std_logic_vector(monExp_SIZE-1 downto 0);
signal monpro_0_B                                   : std_logic_vector(monExp_SIZE-1 downto 0);
signal monpro_0_out                                 : std_logic_vector(monExp_SIZE-1 downto 0);
signal monpro_0_done                                : std_logic;
signal monpro_0_go                                  : std_logic;
-----------------------------------------------------------------------------
-- Monpro (X_m, X_m)
-----------------------------------------------------------------------------
signal monpro_1_out                                 : std_logic_vector(monExp_SIZE-1 downto 0);
signal monpro_1_done                                : std_logic;
signal monpro_1_go                                  : std_logic;
-----------------------------------------------------------------------------
-- Monpro (M_m, X_m)
-----------------------------------------------------------------------------
signal monpro_2_out                                 : std_logic_vector(monExp_SIZE-1 downto 0);
signal monpro_2_done                                : std_logic;
signal monpro_2_go                                  : std_logic;
-----------------------------------------------------------------------------
-- Monpro (X_m, 1)
-----------------------------------------------------------------------------
signal monpro_3_B                                   : std_logic_vector(monExp_SIZE-1 downto 0);
signal monpro_3_out                                 : std_logic_vector(monExp_SIZE-1 downto 0);
signal monpro_3_done                                : std_logic;
signal monpro_3_go                                  : std_logic;

signal monProResets                                 : std_logic;
begin

msgin_ready <= msgin_ready_temp;

-----------------------------------------------------------------------------
-- SNAP
-----------------------------------------------------------------------------
process(clk, reset_n) begin
if(reset_n = '1') then
    msgout_last <= '0';
    msgin_data_temp <= (others => '0');
elsif(rising_edge(clk)) then
    if(msgin_ready_temp = '1' and msgin_valid = '1') then
        msgin_data_temp <= msgin_data;
            if(msgin_last = '1') then
                msgout_last <= '1';
            else
                msgout_last <= '0';
            end if;
    end if;
end if;
end process;

-----------------------------------------------------------------------------
-- FSM
-----------------------------------------------------------------------------
MainStateMachine : entity work.modExp_fsm
port map(clk => clk, reset => reset_n,
initalModDone => monpro_0_done, monPro_1_done => monpro_1_done, monPro_2_done => monPro_2_done,monPro_final_done => monpro_3_done,
ei => key_ei(monExp_SIZE-1), 
msgin_valid => msgin_valid, msgin_ready => msgin_ready_temp, msgout_valid => msgout_valid, msgout_ready => msgout_ready,
initial_mod_go => monpro_0_go, monPro_1_go => monPro_1_go, monPro_2_go => monPro_2_go, monPro_final_go => monpro_3_go,
enableMainCounter => enableMainCounter, resetMainCounter => resetMainCounter, finalCount_Done => final_cnt_dne,
xmreg_rmodn => xmreg_rmodn, resetAllMonPro => monProResets);

-- Counter
MainCounter : entity work.Counter256(DownCount)
generic map(max => monExp_SIZE)
port map(clk => clk, reset => resetMainCounter, enabled => enableMainCounter, done => final_cnt_dne, cnt => cnt);

-- ModPro Blocks
initialMonPro : entity work.modProTop
generic map(k => monExp_SIZE)
port map(clk => clk, reset => monProResets, go => monpro_0_go, n => key_n, A => msgin_data_temp, B => key_rrmodn, U_out => monpro_0_out, modProDone => monpro_0_done);

-- ModPro Blocks
monPro_1 : entity work.modProTop
generic map(k => monExp_SIZE)
port map(clk => clk, reset => monProResets, go => monPro_1_go, n => key_n, A => xm_reg, B => xm_reg, U_out => monpro_1_out, modProDone => monpro_1_done);

-- ModPro Blocks
monPro_2 : entity work.modProTop
generic map(k => monExp_SIZE)
port map(clk => clk, reset => monProResets, go => monPro_2_go, n => key_n, A => monpro_0_out, B => xm_reg, U_out => monpro_2_out, modProDone => monpro_2_done);

monpro_3_B <= std_logic_vector(to_unsigned(1,key_rrmodn'length));


-- ModPro Blocks
finalMonPro : entity work.modProTop
generic map(k => monExp_SIZE)
port map(clk => clk, reset => monProResets, go => monpro_3_go, n => key_n, A => xm_reg, B => monpro_3_B, U_out => monpro_3_out, modProDone => monpro_3_done);

process(clk, reset_n)
    variable xm_var : std_logic_vector(monExp_SIZE-1 downto 0);
begin
    if(reset_n = '1') then
            xm_var := (others => '0');
    elsif(rising_edge(CLK)) then
        -- When to set rmodn to register
        if(xmreg_rmodn = '1' and msgin_valid = '1') then
            xm_var := key_rmodn;
        end if;
        -- Mid case
        if(monpro_3_done = '1') then
            if(key_ei(monExp_SIZE-1) = '1') then
                xm_var := monpro_2_out;
            elsif(key_ei(monExp_SIZE-1) = '0') then
                xm_var := monpro_1_out;
            end if; 
        else
            if(monpro_2_done = '1') then
               xm_var := monpro_2_out;
            elsif(monpro_1_done = '1') then
                xm_var := monpro_1_out;
            end if;
        end if;
             xm_reg <= xm_var;  
    end if;  

end process;

-- X_reg
--process(all) begin
--    if (rising_edge(CLK)) then
--        if(reset_n = '1' or (xmreg_rmodn = '1' and msgin_valid = '1')) then
--            xm_reg <= key_rmodn;
--         else/*if(monpro_1_done = '1' or monpro_2_done = '1' or monpro_3_done = '1') then */
--            xm_reg <= xm_reg_new;
--         end if;
--    end if;
--end process;

-- After monpro, if The final stage of monpro is done, send output to final monpro else store it in register.
--process(all) begin
--    if (reset_n = '1' or (xmreg_rmodn = '1' and msgin_valid = '1')) then
--        xm_reg_new <= key_rmodn;
--    else
--        case(monpro_3_done) is
--        when '1' =>
--            if(key_ei(monExp_SIZE-1) = '1') then 
--                xm_reg_new <= monpro_2_out;
--            else
--                xm_reg_new <= monpro_1_out;
--            end if;
--        when '0' =>
--            if(/*key_ei(monExp_SIZE-1) = '1' and*/ monpro_2_done = '1') then 
--                xm_reg_new <= monpro_2_out;
--            elsif(monpro_1_done = '1') then
--                xm_reg_new <= monpro_1_out;
--            end if;
--        when others =>
--                xm_reg_new <= (others => '-');
--        end case;
--    end if;
--end process;


-- shift e register to the left
key_ei <= std_logic_vector(shift_left(unsigned(key_e_d), to_integer(unsigned((monExp_SIZE-1)-cnt))));
--xm_reg_new <= (others => '1');
msgout_data <= monpro_3_out;

end Behavioral;
