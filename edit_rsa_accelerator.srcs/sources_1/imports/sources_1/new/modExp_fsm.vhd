----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 11/09/2018 10:53:53 AM
-- Design Name: 
-- Module Name: modExp_fsm - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;


entity modExp_fsm is
Port (
---------------------------------------------------
---         Clock and reset 
---------------------------------------------------
clk, reset                                   : in std_logic;
---------------------------------------------------
---         Mon Pro states/Counter
---------------------------------------------------
initalModDone                                 : in std_logic;

monPro_1_done                                 : in std_logic;
monPro_2_done                                 : in std_logic;
monPro_final_done                             : in std_logic;
finalCount_Done                               : in std_logic;


resetAllMonPro                                : out std_logic;
---------------------------------------------------
---         The 255 bit of e shift bit
---------------------------------------------------
ei                                            : in std_logic;                  
---------------------------------------------------
---           Slave -> Master
---------------------------------------------------
msgin_valid                                  : in std_logic; -- is data ready?
msgin_ready                                  : out std_logic; -- we are ready to recieve
---------------------------------------------------
---           Master -> slave
---------------------------------------------------
msgout_valid                                 : out std_logic;   -- we have ready data to send
msgout_ready                                 : in std_logic; -- are they ready to recieve?
---------------------------------------------------
---         Output of statemachine
---------------------------------------------------
initial_mod_go                               : out std_logic;
monPro_1_go                                  : out std_logic;
monPro_2_go                                  : out std_logic;
monPro_final_go                              : out std_logic;
xmreg_rmodn                                  : out std_logic;
resetMainCounter                             : out std_logic;
enableMainCounter                            : out std_logic);
end modExp_fsm;

architecture Behavioral of modExp_fsm is
    type STATES is (idle, initial, monProMain, calcMon1, calcMon2, done, transmitFinal, recieveNewData);
    signal state_reg, state_next: STATES;
begin

process(clk, reset)
begin
    if (clk'event and clk = '1') then
        if (reset = '1') then
            state_reg <= idle;
        else
            state_reg <= state_next;
        end if;
    end if;
end process;

process(state_reg,msgin_valid,initalModDone,finalCount_Done,monPro_1_done,monPro_2_done,monPro_final_done,msgout_ready,ei) begin
    case state_reg is
        when idle =>
            if (msgin_valid = '1') then
                state_next <= initial;
            else
                state_next <= idle;
            end if;
        when initial => 
            if (initalModDone = '1') then
                state_next <= monProMain;
            else
                state_next <= initial;
            end if;
        when monProMain => 
            if(finalCount_Done = '1') then
                state_next <= done;
            else
                state_next <= calcMon1;
            end if;
        when calcMon1 => 
            if(monPro_1_done = '1' and ei = '1') then
                state_next <= calcMon2;
            elsif(monPro_1_done = '1' and ei = '0') then
                state_next <= monProMain;
            end if;
        when calcMon2 => 
            if(monPro_2_done = '1') then
              state_next <= monProMain;
            else
                state_next <= calcMon2;
            end if;    
        when done =>  
            if(monPro_final_done = '1') then
               state_next <= transmitFinal;
            else
                state_next <= done;
            end if;   
        when transmitFinal =>
            if(msgout_ready = '1') then
                state_next <= recieveNewData;
            else
                state_next <= transmitFinal;
            end if;
        when recieveNewData =>
                state_next <= idle;
        when others =>
                state_next <= idle;
    end case;
end process;
-- MSG out last

--process(all)
--    variable msgout_last_temp : std_logic := '0';
--begin
--if(rising_edge(clk)) then
--    if(msgin_last = '1' and ) then
--        msgout_last_temp := '1';
--    elsif(state_reg = transmitFinal) then
--        msgout_last <= msgout_last_temp;
--    end if;
--end if;
--end process;



xmreg_rmodn <= '1' when (state_reg = transmitFinal and msgout_ready = '1') or state_reg = idle else '0';
---------------------------------------------------
---          Counter
---------------------------------------------------
resetMainCounter <= '1' when state_reg = idle else '0';
enableMainCounter <= '1' when state_reg = monProMain else '0';
---------------------------------------------------
---          MonPro related states
---------------------------------------------------
resetAllMonPro <= '1' when (state_reg = idle) or reset = '1' else '0';
initial_mod_go <= '1' when state_reg = initial else '0';
monPro_1_go <= '1' when state_reg = calcMon1 else '0';
monPro_2_go <= '1' when state_reg = calcMon2 else '0';
monPro_final_go <= '1' when state_reg = done else '0';
---------------------------------------------------
---          Handshaking states
---------------------------------------------------
msgin_ready <= '1' when (state_reg = idle and reset = '0') else '0';
-- Final stage of output
msgout_valid <= '1' when state_reg = transmitFinal else '0';

end Behavioral;
