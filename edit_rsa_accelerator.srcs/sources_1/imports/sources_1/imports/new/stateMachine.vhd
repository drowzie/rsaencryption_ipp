----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 10/31/2018 11:46:28 AM
-- Design Name: 
-- Module Name: stateMachine - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity stateMachine is    
          Port (clk, reset, beginCount, MonPro_Done : in std_logic;
          monPro_Done_fsm,monPro_Enable : out std_logic);
end stateMachine;

architecture Behavioral of stateMachine is
    type STATES is (start, counting, done);
    signal state_reg, state_next: STATES;
    signal sig_Dne : std_logic;
begin

    
-- update state reg
process(clk, reset)
begin
    if (clk'event and clk = '1') then
        if (reset = '1') then
            state_reg <= start;
        else
            state_reg <= state_next;
        end if;
    end if;
end process;

process(state_reg, beginCount, MonPro_Done) begin
    case state_reg is
        -- Default state
        -- Reset values when enabled.
        when start =>
            if(beginCount = '1') then
                state_next <= counting;
            end if;
        when counting =>
            if(MonPro_Done = '1') then
                state_next <= done;
                -- when reset is enabled go to default state
            end if;               
        when done => -- Unused, can be removed
            state_next <= start;          
    end case;
end process;

monPro_Done_fsm <= '1' when (state_reg = counting and MonPro_Done = '1') else '0';
monPro_Enable <= '1' when ((state_reg = counting or beginCount = '1') and MonPro_Done = '0') else '0';



end Behavioral;
