----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 10/29/2018 09:02:12 AM
-- Design Name: 
-- Module Name: counter256 - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
use ieee.std_logic_unsigned.all;

entity counter256 is
    Generic(
    n : integer := 8;
    max : integer := 256
    );
    Port ( clk, reset, enabled: in STD_LOGIC;
           cnt : out std_logic_vector(8 downto 0);
           done: out std_logic);
end counter256;

architecture DownCount of counter256 is

signal nxt: std_logic_vector(n downto 0);

begin
    process(reset,clk, enabled) begin
    if (rising_edge(CLK)) then
        if reset='1' then
            nxt <= std_logic_vector(to_unsigned(max,nxt'length));
            done <= '0';
        else
            if(unsigned(nxt) = 0) then
                     done <= '1';
            elsif(enabled='1') then
                     nxt <= nxt - 1;
            end if;
        end if;
    end if;   
    end process;
cnt <= nxt;
end DownCount;
