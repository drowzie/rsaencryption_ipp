----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 10/31/2018 12:58:22 PM
-- Design Name: 
-- Module Name: topModule - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity modProTop is
    Generic (k : integer := 256);
    Port ( clk, reset, go : in std_logic;
           A : in STD_LOGIC_VECTOR(k-1 downto 0);
           B : in STD_LOGIC_VECTOR(k-1 downto 0);
           n : in STD_LOGIC_VECTOR(k-1 downto 0);
           U_out : out STD_Logic_vector(k-1 downto 0);
           modProDone : out std_logic);
end modProTop;

architecture Behavioral of modProTop is
signal monPro_Enable, startMod, reset_Cnt, start_Cnt, MonPro_Done: std_logic;
begin

modPro : entity work.ModPro
    generic map(k => k)
    port map(clk => clk, reset => reset,
             MonPro_Done => MonPro_Done, 
             monPro_Enable => monPro_Enable,             
             A => A, B => B, n => n, U_out => U_out); -- Ports
             
stateMachine : entity work.stateMachine
    port map(clk => clk,
                 reset => reset,
                 MonPro_Done => MonPro_Done,
                 monPro_Enable => monPro_Enable,
                 monPro_Done_fsm => modProDone, 
                 beginCount => go);
end Behavioral;
