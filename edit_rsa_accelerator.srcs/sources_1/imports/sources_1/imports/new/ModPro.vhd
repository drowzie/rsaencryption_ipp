----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 10/29/2018 08:45:48 AM
-- Design Name: 
-- Module Name: ModPro - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
use ieee.std_logic_unsigned.all;

entity ModPro is
    Generic (k : integer := 256);
    Port ( clk, reset,MonPro_Enable: in STD_LOGIC;
           A : in STD_LOGIC_VECTOR(k-1 downto 0);
           B : in STD_LOGIC_VECTOR(k-1 downto 0);
           n : in STD_LOGIC_VECTOR(k-1 downto 0);
           U_out : out STD_Logic_vector(k-1 downto 0);
           MonPro_Done : out std_logic);
end ModPro;

architecture Behavioral of ModPro is
      signal cnt : unsigned(9 downto 0) := (others => '0');
begin     


---------------------------------------------------
--- MonPro Block for calculating montgomery algorithm based by the binary add-shift algorithm
--- Algorithm is taken from RSA_HARDWARE_IMPLEMENTATION.PDF         
---------------------------------------------------
u_reg : process(clk, reset)
    variable u_reg : std_logic_vector(k+2 downto 0) := (others => '0');
begin
    if(reset = '1') then
        U_out <= (others => '1');
        MonPro_Done <= '0';
        cnt <= (others => '0');
        u_reg := (others => '0');
    elsif(rising_edge(clk)) then
         if(MonPro_Enable = '1') then
            if(A(to_integer(unsigned(cnt))) = '1') then
                u_reg := u_reg + B;
            end if;
            
            if(u_reg(0) = '1') then
                u_reg := u_reg + n;
            end if;
            
            u_reg :=  '0' & u_reg(k+2 downto 1);
            
            if(cnt = k-1) then
                if(u_reg > n) then
                    u_reg := u_reg - n;
                end if;
                U_out <= u_reg(k-1 downto 0);
                MonPro_Done <= '1';
            end if;
            cnt <= cnt + 1;
        else
            u_reg := (others => '0'); 
            cnt <= (others => '0'); 
            MonPro_Done <= '0';                   
        end if;           
    end if;
end process;

end Behavioral;
